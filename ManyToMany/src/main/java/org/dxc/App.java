package org.dxc;

import java.util.ArrayList;

import org.dxc.entity.AnswerMtM;
import org.dxc.entity.QuestionMtM;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

// Can try this https://www.javatpoint.com/hibernate-one-to-many-mapping-using-annotation-example
// or this https://www.digitalocean.com/community/tutorials/hibernate-one-to-many-mapping-annotation
public class App 
{
	private static SessionFactory factory;
    public static void main( String[] args )
    {
    	try {
			factory = new Configuration().configure().buildSessionFactory();
		} catch (Throwable e) {
			System.err.println("Failed to create Session Object " + e);
			throw new ExceptionInInitializerError(e);
		}
    	
    	Session session=factory.openSession();
    	Transaction tx = session.beginTransaction();
    	
    	AnswerMtM ans1 =  new AnswerMtM();
    	ans1.setAnswername("Java is a programming language");
    	ans1.setPostedBy("John");
    	
    	AnswerMtM ans2 = new AnswerMtM();
    	ans2.setAnswername("Java is a platform");
    	ans2.setPostedBy("Brian");
    	
    	AnswerMtM ans3 = new AnswerMtM();
    	ans3.setAnswername("Servlet is an interface");
    	ans3.setPostedBy("Haikal");

    	AnswerMtM ans4 = new AnswerMtM();
    	ans4.setAnswername("Servlet is an API");
    	ans4.setPostedBy("Edmund");
    	
    	AnswerMtM ans5 = new AnswerMtM();
    	ans5.setAnswername("Java supports OOPS");
    	ans5.setPostedBy("Damien");
    	
    	ArrayList<AnswerMtM> list1 = new ArrayList<AnswerMtM>();
    	list1.add(ans1); 
    	list1.add(ans2); 
    	list1.add(ans4); 
    	list1.add(ans5);
    	
    	ArrayList<AnswerMtM> list2 = new ArrayList<AnswerMtM>();
    	list2.add(ans3); 
    	list2.add(ans4); 
    	list2.add(ans5);
    	
    	QuestionMtM QuestionMtM1 = new QuestionMtM();
    	QuestionMtM1.setQname("What is java?");
    	QuestionMtM1.setAnswerMtM(list1);

    	QuestionMtM QuestionMtM2 = new QuestionMtM();
    	QuestionMtM2.setQname("What is a Servlet?");
    	QuestionMtM2.setAnswerMtM(list2);

    	session.persist(QuestionMtM1);
    	session.persist(QuestionMtM2);
    	
    	tx.commit();
    	session.close();
    	System.out.println("Success");
    	
    }
    
}
