package org.dxc;

import org.dxc.entity.Employee;
import org.dxc.entity.Address;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

public class App 
{
	private static SessionFactory factory;
	
    public static void main( String[] args )
    {
    	try {
			factory = new Configuration().configure().buildSessionFactory();
		} catch (Throwable e) {
			System.err.println("Failed to create Session Object " + e);
			throw new ExceptionInInitializerError(e);
		}
    	
    	Session session=factory.openSession();
    	Transaction tx = session.beginTransaction();
    	
    	Employee e1 = new Employee();
    	e1.setName("John Bob");
    	e1.setEmail("johnbob@gmail.com");
    	
    	Address a1 = new Address();
    	a1.setAddressLine("Jurong West St 24");
    	a1.setCountry("Singapore");
    	a1.setPostalcode(555321);
    	
    	e1.setAddress(a1);
    	a1.setEmployee(e1);
    	
    	session.persist(e1);   
    	
    	Employee e2 = new Employee();
    	e2.setName("Larry Bird");
    	e2.setEmail("legend@gmail.com");
    	
    	Address a2 = new Address();
    	a2.setAddressLine("Boston Ave");
    	a2.setCountry("USA");
    	a2.setPostalcode(99821);
    	
    	e2.setAddress(a2);
    	a2.setEmployee(e2);
    	
    	session.persist(e2); 
    	
    	tx.commit();
    	session.close();
    	System.out.println("Success");
    }
}
